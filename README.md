## Task 1 

```sql
select users.id as ID,
     concat(users.first_name, ' ', users.last_name) as Name,
     author as Author,
     group_concat(name separator ', ') as Books
from users
       join user_books ub on users.id = ub.user_id
       join books b on b.id = ub.book_id
where age between 7 and 17
group by users.id
having count(book_id) = 2
 and count(distinct author) = 1;
```

DUMP DB:

1. users
```sql
create table users
(
    id         int auto_increment
        primary key,
    first_name text not null,
    last_name  text not null,
    age        int  not null
);

INSERT INTO users (id, first_name, last_name, age) VALUES (1, 'user', '6', 6);
INSERT INTO users (id, first_name, last_name, age) VALUES (2, 'user', '7', 7);
INSERT INTO users (id, first_name, last_name, age) VALUES (3, 'user', '17', 17);
INSERT INTO users (id, first_name, last_name, age) VALUES (4, 'user', '18', 18);
INSERT INTO users (id, first_name, last_name, age) VALUES (5, 'user', '13', 13);
```

2. books
```sql
create table books
(
    id     int auto_increment
        primary key,
    name   text not null,
    author text not null
);

INSERT INTO books (id, name, author) VALUES (1, 'Book 1 1', 'Author 1');
INSERT INTO books (id, name, author) VALUES (2, 'Book 2 1', 'Author 1');
INSERT INTO books (id, name, author) VALUES (3, 'Book 3 1', 'Author 1');
INSERT INTO books (id, name, author) VALUES (4, 'Book 1 2', 'Author 2');
INSERT INTO books (id, name, author) VALUES (5, 'Book 2 2', 'Author 2');
INSERT INTO books (id, name, author) VALUES (6, 'Book 1 3', 'Author 3');
```

3. user_books
```sql
create table user_books
(
    id      int auto_increment
        primary key,
    user_id int not null,
    book_id int not null,
    constraint user_books_books_id_fk
        foreign key (book_id) references books (id)
            on update cascade on delete cascade,
    constraint user_books_users_id_fk
        foreign key (user_id) references users (id)
            on update cascade on delete cascade
);

INSERT INTO user_books (id, user_id, book_id) VALUES (1, 1, 1);
INSERT INTO user_books (id, user_id, book_id) VALUES (2, 1, 2);
INSERT INTO user_books (id, user_id, book_id) VALUES (3, 1, 3);
INSERT INTO user_books (id, user_id, book_id) VALUES (4, 2, 4);
INSERT INTO user_books (id, user_id, book_id) VALUES (5, 2, 5);
INSERT INTO user_books (id, user_id, book_id) VALUES (7, 3, 1);
INSERT INTO user_books (id, user_id, book_id) VALUES (8, 3, 2);
INSERT INTO user_books (id, user_id, book_id) VALUES (10, 4, 4);
INSERT INTO user_books (id, user_id, book_id) VALUES (11, 4, 5);
INSERT INTO user_books (id, user_id, book_id) VALUES (12, 5, 1);
INSERT INTO user_books (id, user_id, book_id) VALUES (13, 5, 4);
```

## Task 2

1. Raise the application in docker

```
docker-compose up -d
```

```
docker-compose run --rm composer install
```

2. Copy .env file

```
cp .env.example .env
```

3. Sync rates
```
docker-compose run --rm artisan rate:sync
```

4. Go to rest endpoint
```
GET /api/v1/rates HTTP/1.1
Host: localhost
Authorization: Bearer ge5L98iXs5PjEGeJBh6cK3rT8asmLbDKvEigudXBNOHA3gspvnA9SSqY5vlJEAsj

###
```

```
POST /api/v1/convert HTTP/1.1
Host: localhost
Authorization: Bearer ge5L98iXs5PjEGeJBh6cK3rT8asmLbDKvEigudXBNOHA3gspvnA9SSqY5vlJEAsj

###
```

5. Tests

```
docker-compose run --rm php ./vendor/bin/phpunit
```
