<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Rate;
use App\Repositories\RateRepository;
use App\Services\CommissionCalculator\CommissionCalculator;
use App\Services\RateFetcher\RateFetcher;
use Illuminate\Console\Command;

class SyncRatesCommand extends Command
{
    /** @var string */
    protected $signature = 'rate:sync';

    /** @var RateFetcher */
    private $fetcher;

    /** @var RateRepository */
    private $repository;

    /** @var CommissionCalculator */
    private $calculator;

    public function __construct(RateFetcher $fetcher, RateRepository $repository, CommissionCalculator $calculator)
    {
        parent::__construct();

        $this->fetcher = $fetcher;
        $this->repository = $repository;
        $this->calculator = $calculator;
    }

    public function handle(): void
    {
        $ratesInfo = $this->fetcher->fetch();

        Rate::truncate();

        foreach ($ratesInfo as $info) {
            $rate = new Rate([
                'currency' => $info->getCurrency(),
                'rate' => $this->calculator->calculate($info->getLast()),
            ]);

            $this->repository->store($rate);
        }
    }
}
