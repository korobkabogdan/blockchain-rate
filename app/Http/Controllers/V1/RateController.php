<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\Repositories\RateRepository;
use App\Resources\RateResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RateController
{
    /** @var RateRepository */
    private $repository;

    public function __construct(RateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $rates = $this->repository->findFromRequest($request);

        $resource = new RateResource($rates);

        return new JsonResponse($resource);
    }
}
