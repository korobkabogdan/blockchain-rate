<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\ConvertRequest;
use App\Models\Convert;
use App\Repositories\ConvertRepository;
use App\Resources\ConvertResource;
use App\Services\CurrencyConverter\ConvertException;
use App\Services\CurrencyConverter\ConvertInfo;
use App\Services\CurrencyConverter\CurrencyConverter;
use Illuminate\Http\JsonResponse;

class ConvertController extends Controller
{
    /** @var CurrencyConverter */
    private $converter;

    /** @var ConvertRepository */
    private $repository;

    public function __construct(CurrencyConverter $converter, ConvertRepository $repository)
    {
        $this->converter = $converter;
        $this->repository = $repository;
    }

    public function __invoke(ConvertRequest $request): JsonResponse
    {
        $convertInfo = new ConvertInfo(
            $request->getCurrencyFrom(),
            $request->getCurrencyTo(),
            $request->getValue()
        );


        try {
            $convertInfo = $this->converter->convert($convertInfo);
        } catch (ConvertException $e) {
            return new JsonResponse([
                'status' => 'error',
                'code' => 500,
                'message' => 'Error convert'
            ], 500);
        }

        $convert = new Convert([
            'currency_from' => $convertInfo->getCurrencyFrom(),
            'currency_to' => $convertInfo->getCurrencyTo(),
            'value' => $convertInfo->getValue(),
            'converted_value' => $convertInfo->getConvertedValue(),
            'rate' => $convertInfo->getRate(),
        ]);

        $this->repository->store($convert);

        $resource = new ConvertResource($convert);

        return new JsonResponse($resource);
    }
}
