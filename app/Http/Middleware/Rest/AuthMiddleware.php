<?php

declare(strict_types=1);

namespace App\Http\Middleware\Rest;

use Closure;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthMiddleware
{
    /**  @throws Exception */
    public function handle(Request $request, Closure $next)
    {
        $token = config('api.auth_token');

        $header = $request->headers->get('Authorization');

        if ($header === "Bearer {$token}") {
            return $next($request);
        }

        return new JsonResponse([
            'status' => 'error',
            'message' => 'Invalid token',
            'code' => 403,
        ], 403);
    }
}
