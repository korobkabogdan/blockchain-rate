<?php

declare(strict_types=1);

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;

class ConvertRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'currency_from' => 'required|string',
            'currency_to' => 'required|string',
            'value' => 'required|numeric|min:0.01|regex:/^\d+(\.\d{1,2})?$/',
        ];
    }

    public function getCurrencyFrom(): string
    {
        return $this->input('currency_from');
    }

    public function getCurrencyTo(): string
    {
        return $this->input('currency_to');
    }

    public function getValue(): string
    {
        return $this->input('value');
    }
}
