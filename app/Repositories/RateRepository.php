<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Rate;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class RateRepository
{
    public function store(Rate $rate): void
    {
        $rate->save();
    }

    public function findByCurrency(string $currency): ?Rate
    {
        return Rate::query()
            ->where('currency', $currency)
            ->first();
    }

    /**
     * @param Request $request
     * @return Collection|Rate[]
     */
    public function findFromRequest(Request $request): Collection
    {
        return QueryBuilder::for(Rate::class, $request)
            ->allowedFilters('currency')
            ->orderBy('rate')
            ->get();
    }
}
