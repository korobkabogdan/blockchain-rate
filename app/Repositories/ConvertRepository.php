<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Convert;

class ConvertRepository
{
    public function store(Convert $rate): void
    {
        $rate->save();
    }
}
