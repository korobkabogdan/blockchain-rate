<?php

declare(strict_types=1);

namespace App\Services\RateFetcher;

class RateInfo
{
    /** @var string */
    private $currency;

    /** @var float */
    private $delay15m;

    /** @var float */
    private $last;

    /** @var float */
    private $buy;

    /** @var float */
    private $sell;

    /** @var string */
    private $symbol;

    public function __construct(string $currency, float $delay15m, float $last, float $buy, float $sell, string $symbol)
    {
        $this->currency = $currency;
        $this->delay15m = $delay15m;
        $this->last = $last;
        $this->buy = $buy;
        $this->sell = $sell;
        $this->symbol = $symbol;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getDelay15m(): float
    {
        return $this->delay15m;
    }

    public function getLast(): float
    {
        return $this->last;
    }

    public function getBuy(): float
    {
        return $this->buy;
    }

    public function getSell(): float
    {
        return $this->sell;
    }

    public function getSymbol(): string
    {
        return $this->symbol;
    }
}
