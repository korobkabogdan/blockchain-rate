<?php

declare(strict_types=1);

namespace App\Services\RateFetcher;

use App\Clients\BlockchainClient;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Arr;
use JsonException;

class RateFetcher
{
    /** @var BlockchainClient */
    private $client;

    public function __construct(BlockchainClient $client)
    {
        $this->client = $client;
    }

    /**
     * @return array<RateInfo>
     * @throws FetchRateException
     */
    public function fetch(): array
    {
        try {
            $response = $this->client->get('ticker');
            $currenciesInfo = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        } catch (GuzzleException|JsonException $e) {
            throw new FetchRateException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }

        $rates = [];
        foreach ($currenciesInfo as $currency => $info) {
            $rates[] = new RateInfo(
                $currency,
                Arr::get($info, '15m'),
                Arr::get($info, 'last'),
                Arr::get($info, 'buy'),
                Arr::get($info, 'sell'),
                Arr::get($info, 'symbol'),
            );
        }

        return $rates;
    }
}
