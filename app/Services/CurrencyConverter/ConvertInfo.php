<?php

declare(strict_types=1);

namespace App\Services\CurrencyConverter;

class ConvertInfo
{
    /** @var string */
    private $currencyFrom;

    /** @var string */
    private $currencyTo;

    /** @var string */
    private $value;

    /** @var string */
    private $convertedValue;

    /** @var string */
    private $rate;

    public function __construct(
        string $currencyFrom,
        string $currencyTo,
        string $value
    ) {
        $this->currencyFrom = $currencyFrom;
        $this->currencyTo = $currencyTo;
        $this->value = $value;
    }

    public function isConverted(): bool
    {
        return $this->convertedValue !== null && $this->rate !== null;
    }

    public function addConvertedValue(string $convertedValue, string $rate): void
    {
        $this->convertedValue = $convertedValue;
        $this->rate = $rate;
    }

    public function getCurrencyFrom(): string
    {
        return $this->currencyFrom;
    }

    public function getCurrencyTo(): string
    {
        return $this->currencyTo;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getConvertedValue(): string
    {
        return $this->convertedValue;
    }

    public function getRate(): string
    {
        return $this->rate;
    }
}
