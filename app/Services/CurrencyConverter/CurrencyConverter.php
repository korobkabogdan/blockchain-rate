<?php

declare(strict_types=1);

namespace App\Services\CurrencyConverter;

use App\Repositories\RateRepository;
use Exception;

class CurrencyConverter
{
    /** @var RateRepository */
    private $repository;

    public function __construct(RateRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param ConvertInfo $info
     * @return ConvertInfo
     *
     * @throws ConvertException
     */
    public function convert(ConvertInfo $info): ConvertInfo
    {
        return $info->getCurrencyFrom() === 'BTC' ? $this->calculateFromBtc($info) : $this->calculateToBtc($info);
    }

    /**
     * @param ConvertInfo $info
     * @return ConvertInfo
     *
     * @throws ConvertException
     */
    private function calculateToBtc(ConvertInfo $info): ConvertInfo
    {
        $rate = $this->repository->findByCurrency($info->getCurrencyFrom());

        if ($rate === null) {
            throw new ConvertException('Rate not found.');
        }

        $rate = (string)$rate->rate;

        $convertedValue = bcdiv($info->getValue(), $rate, 10);

        $info->addConvertedValue($convertedValue, $rate);

        return $info;
    }

    /**
     * @param ConvertInfo $info
     * @return ConvertInfo
     *
     * @throws ConvertException
     */
    private function calculateFromBtc(ConvertInfo $info): ConvertInfo
    {
        $rate = $this->repository->findByCurrency($info->getCurrencyTo());

        if ($rate === null) {
            throw new ConvertException('Rate not found.');
        }

        $rate = bcdiv('1', (string)$rate->rate, 10);
        $convertedValue = bcdiv($info->getValue(), $rate, 2);

        $info->addConvertedValue($convertedValue, $rate);

        return $info;
    }
}
