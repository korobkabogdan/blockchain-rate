<?php

declare(strict_types=1);

namespace App\Services\CommissionCalculator;

class CommissionCalculator
{
    private const PERCENT = 2;

    public function calculate(float $value): float
    {
        return round($value + ($value * self::PERCENT / 100), 2);
    }
}
