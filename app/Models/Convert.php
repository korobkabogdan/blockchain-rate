<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Convert
 *
 * @property int $id
 * @property string $currency_from
 * @property string $currency_to
 * @property string|float $value
 * @property string|float $converted_value
 * @property string|float $rate
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Rate newModelQuery()
 * @method static Builder|Rate newQuery()
 * @method static Builder|Rate query()
 * @mixin \Eloquent
 */
class Convert extends Model
{
    public const CURRENCY_BTC = 'BTC';

    protected $fillable = ['currency_from', 'currency_to', 'value', 'converted_value', 'rate'];

    public function getRateAttribute($value): string
    {
        if ($this->isConvertFromBTC()) {
            return $value;
        }

        return round($value, 2);
    }

    public function getConvertedValueAttribute($value): string
    {
        if ($this->isConvertFromBTC()) {
            return round($value, 2);
        }

        return $value;
    }

    public function isConvertFromBTC(): bool
    {
        return $this->currency_from === self::CURRENCY_BTC;
    }
}
