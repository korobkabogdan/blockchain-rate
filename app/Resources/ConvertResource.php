<?php

declare(strict_types=1);

namespace App\Resources;

use App\Models\Convert;
use Illuminate\Http\Resources\Json\JsonResource;

class ConvertResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var Convert $this */

        return [
            'currency_from' => $this->currency_from,
            'currency_to' => $this->currency_to,
            'value' => $this->value,
            'converted_value' => $this->converted_value,
            'rate' => $this->rate,
            'created_at' => $this->created_at->toDateTimeString(),
        ];
    }
}
