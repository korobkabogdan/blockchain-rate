<?php

declare(strict_types=1);

namespace App\Resources;

use App\Models\Rate;
use Illuminate\Http\Resources\Json\JsonResource;

class RateResource extends JsonResource
{
    public function toArray($request): array
    {
        $result = [];

        /** @var Rate $rate */
        foreach ($this->resource as $rate) {
            $result[$rate->currency] = $rate->rate;
        }

        return $result;
    }
}
