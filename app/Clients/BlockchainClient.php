<?php

declare(strict_types=1);

namespace App\Clients;

use GuzzleHttp\Client;

class BlockchainClient extends Client
{
    public function __construct(array $config = [])
    {
        parent::__construct(array_merge($config, [
            'base_uri' => config('blockchain.uri')
        ]));
    }
}
