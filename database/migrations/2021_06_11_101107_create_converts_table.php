<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConvertsTable extends Migration
{
    public function up(): void
    {
        Schema::create('converts', function (Blueprint $table) {
            $table->id();
            $table->char('currency_from');
            $table->char('currency_to');
            $table->decimal('value', 10, 2);
            $table->decimal('converted_value', 20, 10);
            $table->decimal('rate', 20, 10);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('converts');
    }
}
