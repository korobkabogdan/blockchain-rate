<?php

declare(strict_types=1);

namespace Tests\Unit\Services\RateFetcher;

use App\Clients\BlockchainClient;
use App\Services\RateFetcher\FetchRateException;
use App\Services\RateFetcher\RateFetcher;
use App\Services\RateFetcher\RateInfo;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class RateFetcherTest extends TestCase
{
    /**  @throws \JsonException */
    public function testFetchSuccess(): void
    {
        $data[$currency = 'USD'] = [
            '15m' => $delay15m = 11.11,
            'last' => $last = 22.22,
            'buy' => $buy = 33.33,
            'sell' => $sell = 44.44,
            'symbol' => $symbol = '$',
        ];

        $body = json_encode($data, JSON_THROW_ON_ERROR);

        $handlerStack = HandlerStack::create(new MockHandler([
            new Response(200, [], $body),
        ]));

        $dummyClient = new BlockchainClient([
            'handler' => $handlerStack,
        ]);


        $fetcher = new RateFetcher($dummyClient);

        $rates = $fetcher->fetch();

        self::assertCount(1, $rates);

        $rate = array_shift($rates);

        self::assertInstanceOf(RateInfo::class, $rate);
        self::assertEquals($currency, $rate->getCurrency());
        self::assertEquals($delay15m, $rate->getDelay15m());
        self::assertEquals($last, $rate->getLast());
        self::assertEquals($buy, $rate->getBuy());
        self::assertEquals($sell, $rate->getSell());
        self::assertEquals($symbol, $rate->getSymbol());
    }

    public function testRequestException(): void
    {
        $handlerStack = HandlerStack::create(new MockHandler([
            new RequestException('Error Communicating with Server', new Request('GET', 'test'))
        ]));

        $dummyClient = new BlockchainClient([
            'handler' => $handlerStack,
        ]);

        $fetcher = new RateFetcher($dummyClient);

        $this->expectException(FetchRateException::class);
        $fetcher->fetch();
    }
}
