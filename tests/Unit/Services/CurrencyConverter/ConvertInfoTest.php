<?php

declare(strict_types=1);

namespace Tests\Unit\Services\CurrencyConverter;

use App\Services\CurrencyConverter\ConvertInfo;
use Tests\TestCase;

class ConvertInfoTest extends TestCase
{
    public function testConverted(): void
    {
        $convertInfo = new ConvertInfo('USD', 'BTC', '1.00');
        self::assertFalse($convertInfo->isConverted());

        $convertInfo->addConvertedValue('1.22', '1.33');
        self::assertTrue($convertInfo->isConverted());
    }
}
