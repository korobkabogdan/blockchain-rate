<?php

declare(strict_types=1);

namespace Tests\Unit\Services\CommissionCalculator;

use App\Services\CommissionCalculator\CommissionCalculator;
use Tests\TestCase;

class CommissionCalculatorTest extends TestCase
{
    public function dataProvider(): array
    {
        return [
            [1, 1.02],
            [2, 2.04],
            [3, 3.06],
            [4, 4.08],
            [5, 5.1],
            [6, 6.12],
            [7, 7.14],
            [8, 8.16],
            [9, 9.18],
            [10, 10.2],
        ];
    }


    /**
     * @dataProvider dataProvider
     */
    public function testSuccess(float $value, float $except): void
    {
        $calculator = new CommissionCalculator();

        $price = $calculator->calculate($value);

        self::assertEquals($except, $price);
    }
}
