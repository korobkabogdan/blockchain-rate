<?php

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;

class AuthTest extends TestCase
{
    public function testSuccess(): void
    {
        $token = config('api.auth_token');

        $response = $this->get('/api/v1/auth', [
            'Authorization' => "Bearer {$token}"
        ]);

        $response->assertStatus(200);

        $response->assertJson([
            'status' => 'ok',
            'message' => 'Auth success',
            'code' => 200
        ]);
    }

    public function testNotValidToken(): void
    {
        $token = 'not-valid-token';

        $response = $this->get('/api/v1/auth', [
            'Authorization' => "Bearer {$token}"
        ]);

        $response->assertStatus(403);

        $response->assertJson([
            'status' => 'error',
            'message' => 'Invalid token',
            'code' => 403,
        ]);
    }

    public function testWithoutAuthHeader(): void
    {
        $response = $this->get('/api/v1/auth');

        $response->assertStatus(403);

        $response->assertJson([
            'status' => 'error',
            'message' => 'Invalid token',
            'code' => 403,
        ]);
    }
}
