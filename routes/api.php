<?php

use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(static function (): void {
    Route::get('auth', \App\Http\Controllers\V1\AuthController::class);

    Route::get('rates', \App\Http\Controllers\V1\RateController::class);

    Route::post('convert', \App\Http\Controllers\V1\ConvertController::class);
});
